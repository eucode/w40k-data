var Metalsmith = require("metalsmith");
var markdown = require('metalsmith-markdown');
var mjson = require('metalsmith-json');
// var collections = require('metalsmith-collections');
var useful_metadata = require('./lib/useful-metadata');
var extname_transform = require('./lib/extname-transform');
var layouts = require('metalsmith-layouts');
var assets = require('metalsmith-assets');
var register_helpers = require('metalsmith-register-helpers');

Metalsmith(__dirname)
  .source("./src")
  .use(register_helpers({
    directory: "./helpers"
  }))
  .use(markdown())
  .use(mjson())
  .use(extname_transform({
  	orig: 'json',
  	dest: 'html'
  }))
  //.use(folder_nav("./src/api"))
  .use(useful_metadata())
  .use(layouts({
    engine: "handlebars",
    partials: "partials"
  }))
  .use(assets({
    source: "./assets",
    destination: "./assets"
  }))
  .destination("./public")
  .build(err => { if (err) throw err; })
