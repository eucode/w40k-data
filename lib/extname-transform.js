var basename = require('path').basename
var dirname = require('path').dirname
var extname = require('path').extname
var orig = 'json'

module.exports = plugin;

function plugin (options) {
  options = options || {}
  orig = options.orig || orig
  var dest = options.dest || 'html'

  return function (files, metalsmith, done) {

    Object.keys(files).forEach(function(file) {
      if (!check(file)) return;
      var data = files[file];
      var dir = dirname(file);
      var name = basename(file, extname(file)) + '.' + dest
      if ('.' !== dir) name = dir + '/' + name

      var nobj = JSON.parse(JSON.stringify(data));
      files[name] = nobj
      delete files[file].layout
      delete files[file].collection
    });

    done()
  }
}

function check(file) {
  var re = new RegExp("\\."+orig);
  return re.test(extname(file));
}
