var path = require('path')
module.exports = plugin;

function plugin (options) {
  return function (files, metalsmith, done) {
    var metadata = metalsmith.metadata()
    metadata.origin = 'eucode.gitlab.io/w40k-data';
    metadata.my_collections = {}

    Object.keys(files).forEach(function(file) {
      // Path
      var dir = path.dirname(file)
      var ext = path.extname(file)
      var base = path.basename(file)
      var filename = path.basename(file, ext)
      files[file].path = file
      files[file].directory = dir
      files[file].basename = base
      files[file].filename = filename
      if (ext == '.html' && filename != 'index') {
        if (metadata.my_collections[dir] == undefined) metadata.my_collections[dir] = []
        metadata.my_collections[dir].push(files[file])
      }
    })
    done()
  }
}
