---
title: Fuego de apoyo
collection: rtau7th
layout: rule.hbs
---

*La doctrina de la Casta del Fuego, tal como figura en el Código del Fuego, dicta que cada guerrero ha de proteger a sus camaradas. Al superponer sus campos de fuego, los Equipos se dan apoyo mútuo en el campo de batalla.*

Cuando una unidad enemiga declara una carga, todas las miniaturas amigas con esta regla especial de unidades a 6" o menos del objetivo de la unidad que carga pueden efectuar Disparo defensivo como si también fuesen objetivos de la carga. Recuerda que una unidad solo puede efectuar Dispam defensivo una vez por fase.
