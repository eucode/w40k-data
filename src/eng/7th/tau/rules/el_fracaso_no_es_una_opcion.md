---
title: El fracaso no es una opción
collection: rtau7th
layout: rule.hbs
---

*Bajo la mirada mesurada de un Etéreo, un renovado propósito que raya el fanatismo sobrecogce a los Tau. Más la pérdida de tan reverenciado líder en combate les arrojará a la mayor de las angustias.*

Toda unidad de la facción del Imperio Tau amiga a 12" o menos de un Etéreo usa su Liderazgo para los chequeos de Acobardamiento, Miedo, Moral y Reagrupamiento. Sin embargo, si el Etéreo es retirado como baja en una misión en la que se usen puntos de victoria, tu oponente obtiene un punto de victoria adicional.
