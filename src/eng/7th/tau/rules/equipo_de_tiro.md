---
title: Equipo de tiro
collection: rtau7th
layout: rule.hbs
---

*Los sensores de algunas Battlesuits y tanques se pueden entrelazar para aumentar su eficiencia cuando operan como equipos de tiro.*

Mientras una unidad con esta regla especial contenga 3 vehículos o Criaturas monstruosas, todos los vehículos o Criaturas monstruosas de la unidad tienen +1 a la Habilidad de Proyectiles.
