---
title: Ritual Ta'lissera
collection: rtau7th
layout: rule.hbs
---

*Numerosos grupos de Tau se someten a la solemne y sangrienta ceremonia del ta'lissera. Las miembros de un equipo que compartan tal vincula se apoyarán mutuamente a toda costa, hasta la muerte.*

Una unidad formada en su totalidad por miniaturas con esta regla especial (sin contar los Drones) se beneficia de moral heroica incluso si no la acompaña ningún Personaje independiente.
