---
title: Cazatanques
collection: rules7th
layout: rule.hbs
---

*Estos veteranos del combate contra blindados saben identiﬁcar los puntos débiles de los vehículos enemigos y aprovecharse de ellos.*

Una unidad con una o más miniaturas con esta regla especial repite sus tiradas para Penetrar blindaje fallidas contra vehículos, tanto al disparar como en cuerpo a cuerpo, y puede repetir las tiradas que causen impactos superficiales para intentar causar impactos internos, aunque debe quedarse con e] segundo resultado.
