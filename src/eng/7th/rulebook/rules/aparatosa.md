---
title: Aparatosa
collection: rules7th
layout: rule.hbs
---

*Esta arma es muy grande y difícil de manejar, imposibilitando los golpes rápidos con ella.*

Una miniatura que ataque con un arma con esta regla especial se Une al combate
y lucha en el **paso de Iniciativa 1**, excepto si es Criatura Monstruosa o Andador.
