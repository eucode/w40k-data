---
title: Acribillar
collection: rules7th
layout: rule.hbs
---

*El vehículo se diseñó pam atacar blancos temestres: la diïpmión a'g sus dispams se ha calibrado para maximizar la matanza.*

Un vehículo con esta regla especial gana **+1 a su Habilidad de Proyectiles** al disparar armas de Asalto, Pesadas, de Fuego rápido o de Salvas contra: Artillería, Bestias, Motos, Caballería, Infantería, Criaturas Monstruosas y Vehículos que no sean Volador ni Gravítico.
