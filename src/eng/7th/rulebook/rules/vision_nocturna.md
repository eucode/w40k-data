---
title: Visión nocturna
collection: rules7th
layout: rule.hbs
---

*Algunos guerreros son capaces de ver tan claramente en la oscuridad como a plena luz del día.*

Una unidad con una o más miniaturas con esta regla especial ignora los efectos del Combate nocturno.
