---
title: Contraataque
collection: rules7th
layout: rule.hbs
---

*Las tropas con esta habilidad creen que la mejor defensa es un buen ataque. Si son asaltadas, se lanzarán sobre el enemigo y contraatacarán.*

Si una unidad con una o más miniaturas con esta regla especial recibe una carga, cada miniatura con la regla especial Contraataque de la unidad gana **+1 Ataque** hasta el final de la fase.

Si la unidad ya está involucrada en combate cuando recibe 1a carga, esta regla especial no tiene efecto.
