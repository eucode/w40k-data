---
title: Atacar y huir
collection: rules7th
layout: rule.hbs
---

*Ciertas tropas usan una táctica flexibie; golpean al enemigo de cerca y se alejan a1 momento para volver a asaltarle segundos después.*

Una unidad con una o más miniaturas con esta regla espacial e involucrada en combate puede abandonar el cuerpo a cuerpo voluntariamente al final de cualquier fase de Asalto. Si quiere hacerlo debe efectuar un chequeo de Iniciativa.

Si falla el chequeo las miniaturas siguen involucradas en combate.

Si lo supera, elige una dirección y tira 3D6. Si la distancia obtenida en pulgadas basta para que toda la unidad mueva a más de l" de toda unidad enemiga con la que esté involucrada en combate, 1a unidad se destraba del combate y mueve de inmediato tantas pulgadas en la dirección escogida como indique el resultado, ignorando las miniaturas con las que estaba involucrada en combate. No se efectúan Persecuciones arrolladoras. Las unidades enemigas que dejen de estar involucradas en combate Consolidan 1D6" de inmediato.

E1 movimiento de Atacar y huir no se ve ralentizado por el Terreno difícil, pero está sujeto a chequeos de Terreno peligroso del modo habitual. No puede usarse para ponerse en contacto de peana o casco con miniaturas enemigas; las miniaturas se detienen a 1" de ellas.

Si hay unidades con esta regla especial que quieran destrabarse en ambos bandos, determina mediante la tirada más alta quién lo hará en primer lugar y alternaos para destrabarlas. Si la última de ellas ya no está involucrada en combate cuando le toca hacerlo, en lugar de ello, Consolida.
