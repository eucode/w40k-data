---
title: Ataque vectorial
collection: rules7th
layout: rule.hbs
---
*Estos terrores alados surgen de entre las nubes de tormenta, despedazando a sus incautas víctimas de una sola y devastadora pasada a ras de suelo.*

Una miniatura con esta regla especial que esté en Picado o Acelerando puede atacar a su presa. Al final de la fase de Movimiento, elige una unidad enemiga sobre la que haya volado la miniatura este turno y que no esté involucrada en combate. Esa unidad recibe un impacto. Si es un Volador enemigo Acelerando o una Criatura Monstruosa enemiga en Picado, en vez de eso, recibe 1D3 impactos. Si no se especiﬁca lo contrario, los impactos del Ataque vectorial se resuelven con la **Fuerza sin modificar de la miniatura**, **FP 2** y son de **Asignación aleatoria**. Además, tienen la regla especial Ignora cobertura. Estos impactos no se benefician de ninguna regla especial de la miniatura como: ***Asalto rabioso***, ***Envenenado***, ***Acerado***, etc. Si el objetivo es un Vehículo, resuelve estos impactos contra su Blindaje Lateral.

Una miniatura que efectúe un Ataque Vectorial en su fase de Movimiento cuenta como si ya hubiera disparado un arma en su siguiente fase de Disparo. Cualquier otra arma a distancia que use en ese turno puede disparar contra un objetivo diferente al del Ataque vectorial.
