---
title: Acobardamiento
collection: rules7th
layout: rule.hbs
---

*Verse bajo el fuego enemiga sin saber desde dónde disparar o recibir una lluvia de artillería puede minar la moral del guerrero más resoluto, obligándole a tirarse al suelo y parapetarse detrás de cualquier cobertura disponible.*

Si una unidad que no sea Vehículo sufre una o más Heridas no salvadas de un arma con esta regla especial, debe efectuar un chequeo de Liderazgo cuando la unidad tiradora finalice sus ataques de disparo en esa fase. Este chequeo se llama chequeo de Acobardamiento.

Si falla el Chequeo, la unidad queda Acobardada y se echa Cuerpo a tierra de inmediato `(pág. 38)`. Como la unidad ya ha efectuado sus salvaciones, estar Cuerpo a tierra no la protege del arma que causó el chequeo, ya es demasiado tarde.

Mientras lo supere, la unidad puede verse obligada a efectuar varios chequeos de Acobardamiento en un sólo turno, pero sólo uno por cada unidad tiradora. Una unidad que está Cuerpo a tierra no efectúa más chequeos de Acobardamiento.
