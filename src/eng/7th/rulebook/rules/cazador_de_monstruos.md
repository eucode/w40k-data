---
title: Cazador de monstruos
collection: rules7th
layout: rule.hbs
---

*La galaxia es el hogar de muchas bastias colosales, y algunos guerreros del cuadragésimo primer Milenio han pulido sus habilidades con el fin de darles caza.*

Una unidad con una o más miniaturas con esta regla especial repite todas sus tiradas para Herir fallidas contra Criaturas Monstruosas.
