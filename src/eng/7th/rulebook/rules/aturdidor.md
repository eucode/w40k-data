---
title: Aturdidor
collection: rules7th
layout: rule.hbs
---

*Algunas armas están diseñadas para dejar desorientados a los enemigos que sobrevivan a sus ataques, para que sean fáciles de rematar.*

La Iniciativa de una miniatura que sufra una o más Heridas no salvadas de un arma con esta regla especial se reduce a 1 hasta el final de la próxima fase de Asalto.
