---
title: Centinela automatizado
collection: rules7th
layout: rule.hbs
---

*Muchas fortalezas poseen sistemas de defensa automatizados o un espíritu máquina rudimentario que opera sus armas aún en ausencia de una guarnición.*

Un edificio con esta regla especial puede disparar sobre unidades enemigas según la regla ***Disparo automatizado***, incluso si está desocupado. Además, las unidades enemigas pueden disparar y cargar contra un edificio con esta regla especial, incluso si está desocupado.
