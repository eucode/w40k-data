---
title: Arma de especialista
collection: rules7th
layout: rule.hbs
---

*Las armas más poderosas alcanzan su máximo potencial cuando se usan a pares, pues no se usan del mismo modo que armas normales.*

Una miniatura que use un arma con esta regla especial sólo gana **+1 Ataque** por luchar con dos armas si al menos otra de sus armas de Combate tiene esta regla.
