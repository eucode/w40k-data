---
title: Tozudo
collection: rules7th
layout: rule.hbs
---

*Muchos guerreros viven y mueren según la máxima 'muerte antes que deshonor'. Un guerrero así raras veces retrocederá ante el peligro.*

Una unidad con una o más miniaturas con esta regla especial ignora cualquier penalizador a su liderazgo al efectuar chequeos de Acobardamiento o Moral. Si una unidad tiene esta regla especial y Coraje al mismo tiempo, sigue las reglas de Coraje.
