---
title: Veloz
collection: rules7th
layout: rule.hbs
---

*Estos guerreros de agilidad sobrenatural cubren distancias más rápido que sus torpes enemigos.*

Una unidad compuesta sólo por miniaturas con esta regla especial puede **repetir uno o más dados** cuando determine la distancia que corre y su distancia de carga. Por ejemplo, puede repetir un solo dado de su tirada de distancia de carga.
