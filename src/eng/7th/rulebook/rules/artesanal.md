---
title: Artesanal
collection: rules7th
layout: rule.hbs
---

*Algunas armas son artefactos arcanos, fruto de unas habilidades ya olvidadas. Pueden ser de cualquier tipo, pero todas son la obra cumbre de la artesanía de un maestro armero.*

El portador de un arma con esta regla especial puede repetir cada turno una tirada para Impactar fallida con esa arma.
