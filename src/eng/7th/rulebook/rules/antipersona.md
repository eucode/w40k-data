---
title: Antipersona
collection: rules7th
layout: rule.hbs
---

*Hay armas y criaturas cuyo mero roce es letal.*

Una miniatura con esta regla especial, o que ataque con un arma de Combate con esta regla especial, siempre hiere con 2+ en cuerpo a cuerpo.

Del mismo modo, si una miniatura efectúa un ataque de disparo con un arma con esta regla especial, siempre hiere con 2+.

En cualquier caso, esta regla especial no tiene efecto contra Vehículos ni edificios.
