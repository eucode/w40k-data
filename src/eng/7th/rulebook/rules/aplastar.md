---
title: Aplastar
collection: rules7th
layout: rule.hbs
---

*Un golpe de las criaturas más temibles puede atravesar el blindaje de un tanque.*

Todos los ataques en cuerpo a cuerpo de una miniatura con esta regla especial, excepto los ataques de Martillo de furia, se resuelven con **FP 2**, salvo si ataca con un arma de **FP 1**. Además. cuando efectúa sus ataques en cuerpo a cuerpo, puede elegir sustituirlos por un único Ataque Aplastador. Si lo hace, tira para Impactar de modo normal, pero resuelve el Ataque con el doble de Fuerza de la miniatura, hasta un máximo de 10. Además, una miniatura que efectúa un Ataque Aplastador puede repetir sus tiradas para Penetrar blindaje, aunque debe quedarse con el segundo resultado.
