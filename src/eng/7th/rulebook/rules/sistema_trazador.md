---
title: Sistema trazador
collection: rules7th
layout: rule.hbs
---

*Los sofisticados sistemas de puntería de esta unidad le permiten disparar sus proyectiles con gran precisión, por lo que resulta devastadora incluso desde largas distancias*

Una miniatura con esta regla especial repite sus tiradas para impactar fallidas al disparar un arma con la regla especial ***Un solo uso***.

En vez de eso, si una miniatura con esta regla especial dispara un arma con las reglas especiales ***Un solo uso*** y ***Área***, ese disparo se dispersa 1d6" y no 2d6".
