---
title: Rabia
collection: rules7th
layout: rule.hbs
---

*La sed de sangre es una arma poderosa que espolea a los guerreros a destrozar a sus enemigos en un torbellino de violencia irracional, aunque altamente satisfactoria.*

Una miniatura con esta regla especial gana +2 Ataques por cargar, en lugar de +1. Una miniatura que efectúe una carga desordenada no recibe el bonificador de Rabia.
