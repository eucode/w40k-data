---
title: Coraje
collection: rules7th
layout: rule.hbs
---

*Las tropas con coraje nunca se rinden y raras veces se ponen a cubierto, incluso cuando eso sería lo más sensato.*

Una unidad con una o más miniaturas con esta regla especial supera automáticamente sus chequeos de Acobardamiento, Miedo, Reagrupamiento y Moral, pero no puede echarse Cuerpo a tierra ni elegir fallar su chequeo de Moral según la regla Nuestras armas no sirven `(pág. 53)`. Si una unidad Cuerpo a tierra gana la regla especial Coraje, todos los efectos de Cuerpo a tierra se anulan de inmediato.
