---
title: Psíquico
collection: rules7th
layout: rule.hbs
---

*Los Psíquicos son místicos guerreros que canalizan las energías de la Disformidad*

Una miniatura con esta regla especial es un Psíquico. Si no se indica un Nivel de Maestría entre paréntesis junto a la regla, la miniatura tiene Nivel de Maestría 1.
