---
title: Cruzado
collection: rules7th
layout: rule.hbs
---

*Impulsado por una fe absoluta en su objetivo, el cruzado no descansa, arrollando a un enemigo tras otro en una batalla interminable.*

Una unidad con una o más miniaturas con esta regla especial tira un dado adicional para Correr y usa el resultado más alto. Además, una unidad con una o más miniaturas con esta regla especial suma 1D3 al resultado total de sus Persecuciones arrolladoras. Haz una tirada cada vez.
