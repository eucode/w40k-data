---
title: A dos manos
collection: rules7th
layout: rule.hbs
---

*El arma es particularmente pesada y hay que manejarla a dos manos*

Una miniatura que ataque con un arma con esta regla especial nunca gana **+1 Ataque** por luchar con dos armas cuerpo a cuerpo `(pág. 49)`
