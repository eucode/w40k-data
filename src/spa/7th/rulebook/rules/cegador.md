---
title: Cegador
collection: rules7th
layout: rule.hbs
---

*Este ataque descarga un fogonazo brillante que deslumbra a la víctima, obligándola a luchar a ciegas temporalmente.*

Toda unidad impactada por una o más miniaturas o armas con esta regla especial debe efectuar de inmediato
un chequeo de Iniciativa. Si supera el chequeo, todo va bien; un grito previene a los guerreros para que aparten la vista. Si falla el chequeo de Iniciativa, la Habilidad de Armas y Habilidad de Proyectiles de todas las miniaturas de la unidad se reducen a 1 hasta el final de su próximo turno. Si la unidad atacante se impactase a sí misma, se supone que estaban preparados y superan el chequeo automáticamente. Esta regla especial no tiene efecto contra miniaturas sin atributo de Iniciativa, por ejemplo Vehículos que no sean Andadores, edificios, etc.
