---
title: Acerada
collection: rules7th
layout: rule.hbs
---

*Algunas armas infligen impactos críticos contra los que las armaduras no sirven.*

Una miniatura con esta regla especial, o que ataque con un arma de Combate con esta regla especial, puede causar impactos críticos con sus ataques cuerpo a cuerpo. Cada 6 que obtenga para Herir inflige automáticamente una Herida, sea cual sea la Resistencia del objetivo. Resuelve estas Heridas con **FP 2**.

Del mismo modo, si una miniatura efectúa un ataque de disparo con un arma con esta regla especial, cada 6 que obtenga para Herir inflige automáticamente una Herida, sea cual sea la Resistencia del objetivo. Resuelve estas Heridas con **FP 2**.

En cualquier caso, contra Vehículos, cada 6 que obtenga para Penetrar blindaje le permite tirar 1D3 y sumar e] resultado al total. Estos impactos no se resuelven con FP 2, sino que usan el valor de **FP de la miniatura o del arma**.
