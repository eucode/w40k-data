---
title: Asalto rabioso
collection: rules7th
layout: rule.hbs
---

*Algunos guerreros se valen del impulso de la
carga para alimentar su furia.*

Una miniatura con esta regla especial suma **+1 a su Fuerza** hasta el ﬁnal de
la fase de Asalto de un turno en el que cargue al combate. Una miniatura que efectúe una carga desordenada no recibe el bonificador de Asalto rabioso `(pág. 54)`.
