---
title: Voluntad de adamantio
collection: rules7th
layout: rule.hbs
---

*Este guerrero tiene tal fuerza de voluntad que la hechicería apenas le afecta.*

Una unidad que incluya una o más miniaturas con esta regla especial tiene un +1 a sus chequeos de Rechazar a la Bruja.
