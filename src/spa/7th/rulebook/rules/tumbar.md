---
title: Tumbar
collection: rules7th
layout: rule.hbs
---

*Un golpe lo bastante fuerte puede tumbar hasta el guerrero más fornido.*

Cualquier miniatura que no sea Vehículo que sufra una o más Heridas no salvadas, o supere una o más tiradas de salvación, provocadas por un ataque con esta regla especial mueve como si estuviera en Terreno difícil el final de su próximo turno. Puedes indicar con marcadores o unas monedas a qué miniaturas afecta.
