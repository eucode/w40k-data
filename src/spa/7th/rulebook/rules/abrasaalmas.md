---
title: Abrasaalmas
collection: rules7th
layout: rule.hbs
---

*Algunas armas imbuidas con energía psíquíca queman la própia alma de su víctima, consumiéndola en nubes de fuego etéreo.*

Una unidad que sufra una o más Heridas no salvadas infligidas con esta regla especial, prenderá en llamas y arderá; indícalo con un marcador, o una moneda. Al ﬁnal de cada turno, tira 1D6 por cada unidad con un marcador de Abrasaalmas. Con 3 o menos, las llamas se extinguen y la unidad deja de arder; retira el marcador. Con 4+, la unidad recibe 1D3 impactos de **Fuerza 4** y **FP 5** con la regla especial ***Ignora cobertura***. Asigna las Heridas aleatoriamente. Una unidad no puede tener más de un marcador de Abrasaalmas al mismo tiempo.
