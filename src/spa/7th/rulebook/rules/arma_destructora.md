---
title: Arma destructora
collection: rules7th
layout: rule.hbs
---

*Estas armas “matatitanes”, infligen daños inmensos a los blancos sobre los que impactan.*

Un arma con una D en vez de un valor de Fuerza en su perfil es un arma destructora. Para resolver el ataque de un arma destructora, tira para Impaclar igual que con un ataque normal. Si el ataque impacta, tira en la tabla superior en vez de tirar para Herir o para Penetrar blindaje. La mayoría de armas destructoras tienen FP1 o FP2, así que lo habitual es que no haya posibilidad de salvación por armadura contra ellas. Sí que hay posibilidad de salvación invulnerable o por cobertura contra los impactos de un arma destructora del modo habitual, a menos que haya obtenido un resultado de Impacto devastador o Herida mortal. A efectos de determinar si el impacto de un arma destructora tiene la regla especial ***Muerte instantánea***, considera que tiene Fuerza 1O. Las Heridas múltiples y la pérdida de múltiples Puntos de Armazón que inflija un arma destructora no se pueden repartir entre varias miniaturas de la unidad; cualquiera que sobre, se pierde.
