---
title: Baluarte duradero
collection: rules7th
layout: rule.hbs
---

*Algunas fortificaciones son tan enormes y tan sólidas que pueden resistir asedios prólongados y el fuego de las armas superpesadas.*

Un edificio con esta regla especial que sufra un Impacto interno tiene un -1 a la tirada en la Tabla de daños a edificios.
