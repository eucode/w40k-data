---
title: Y no conocerán el miedo
collection: rules7th
layout: rule.hbs
---

*Algunos guerreros se niegan a rendirse y luchan a pesar de cualquir circuntancia.*

Una unidad con una o más miniaturas con esta regla especial supera automáticamente los chequeos de Miedo y de Reagrupamiento. Al reagruparse, la unidad no efectúa el movimiento de reagrupamiento de 3", pero em vez de ello, puede mover, disparar o correr y declarar cargas de modo normal en el que se Reagrupa. Además, si una unidad con una o más miniaturas con esta regla especial es alcanzada por una Persecución arrolladora, no es destruida, sino que sigue involucrada combate.
