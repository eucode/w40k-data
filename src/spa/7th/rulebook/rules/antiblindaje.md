---
title: Antiblindaje
collection: rules7th
layout: rule.hbs
---

*El arma se ha fabricado con un objetivo: atravesar la protección de los vehículos blindados.*

Una miniatura con esta regla especial, o que ataque con un arma de Combate con esta regla especial, tira 1D6 adicional para Penetrar blindaje en cuerpo a cuerpo. Si una miniatura efectúa un ataque de disparo con un arma con esta regla especial, Lira 1DG adicional para Penetrar blindaje. En cualquier caso, esta regla especial no tiene efecto contra miniaturas que no sean Vehículo.
