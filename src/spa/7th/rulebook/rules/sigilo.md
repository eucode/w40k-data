---
title: Sigilo
collection: rules7th
layout: rule.hbs
---

*Algunos guerreros, maestros del disfraz y la ocultación, desaparecen entre las ruinas del campo de batalla hasta el momento de atacar.*

Una unidad con una o más miniaturas con esta regla especial considera su salvación por cobertura 1 punto mejor de lo normal. Ten en cuenta que esto significa que una miniatura con esta regla especial siempre tiene una salvación por cobertura de al menos 6+, incluso en campo abierto.
