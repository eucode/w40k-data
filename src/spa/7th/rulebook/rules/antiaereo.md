---
title: Antiaéreo
collection: rules7th
layout: rule.hbs
---

*Las armas antiaéreas son idóneas para abatir gravíticos y demás aeronaves enemigas.*

Una miniatura con esta regla especial o que dispare un arma con esta regla especial, dispara con su Habilidad de Proyectiles normal contra Voladores, Criaturas Monstruosas Voladoras y Gravíticos, pero contra otros objetivos sólo puede efectuar Disparos apresurados.
