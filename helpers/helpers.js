var path = require('path') 

module.exports = {
  inObjProp: function (ctx, vrb, options) {
    return options.fn(ctx[vrb]);
  },

  eachInObjProp: function (ctx, vrb, options) {
    ret = "";
    for (var i = 0; i < ctx[vrb].length; i++)
      ret = ret + options.fn(context[i]);
    return ret;
  },
  relative: function (current, target) {
    current = path.normalize(current).slice(0);
    target = path.normalize(target).slice(0);

    current = path.dirname(current);
    var out = path.relative(current, target);
    return out;
  }
};
